 #!/bin/bash
export GITLABCI_NAME_DOCKER=runner-docker
export GITLABCI_NAME=runner-shell
export GITLABCI_URL=https://gitlab.com/
export GITLABCI_TOKEN=YOUR TOKEN
export GITLABCI_EXECUTOR=shell
export GITLABCI_TAGS=shell
export GITLABCI_EXECUTOR_DOCKER=docker
export GITLABCI_TAGS_DOCKER=docker

#регистрация shell-раннера
sudo gitlab-runner register \
	--non-interactive \
	--name "$GITLABCI_NAME" \
	--url "$GITLABCI_URL" \
	--registration-token "$GITLABCI_TOKEN" \
	--executor "$GITLABCI_EXECUTOR" \
	--tag-list "$GITLABCI_TAGS"

#добавить gitlab-runner в группу docker
sudo usermod -aG docker gitlab-runner

#остановить и удалить контейнер с раннером, если он уже существует
docker stop gitlab-runner
docker rm gitlab-runner

#запуск раннера
docker run -d --name gitlab-runner --restart always \
   -v /srv/gitlab-runner/config:/etc/gitlab-runner \
   -v /var/run/docker.sock:/var/run/docker.sock \
   gitlab/gitlab-runner:latest

#регистрация docker-раннера
docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
	--non-interactive \
	--name "$GITLABCI_NAME_DOCKER" \
	--url "$GITLABCI_URL" \
	--registration-token "$GITLABCI_TOKEN" \
	--executor "$GITLABCI_EXECUTOR_DOCKER" \
	--tag-list "$GITLABCI_TAGS_DOCKER" \
	--docker-image ubuntu
